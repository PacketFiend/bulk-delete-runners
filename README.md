# Bulk delete runners

Allows to delete all runners in projects or groups that match certain tags or have a contact date before a certain date.

**Please run in dryrun mode (-n) first to ensure no runners are accidentally deleted**

## Usage

```
pip3 install -r requirements.txt
python3 delete_runners.py $GIT_TOKEN -g $GROUP_ID -p $PROJECT_ID --tags $tag1,$tag2 --stale_date 2021-06-07 --no-act --limit $deletion_limit --gitlab $gitlab_url

```

## Parameters

* `token`: An access token with appropriate permissions on the groups or projects to delete runners on. Needs `api` scope.
* `-g`,`--group`: ID of the group to delete runners on. Allows multiple groups: `-g $GROUP1 -g $GROUP2`
* `-p`,`--project`: ID of the project to delete runners on. Allows multiple projects: `-p $PROJECT1 -p $PROJECT2`. You may delete from multiple groups and projects at the same time. Specifying neither group nor project will delete instance runners.
* `--tags`: Optional, comma-separated list of tags. To be deleted, a runner has to have all the tags specified in the tag list. Example: `python3 delete_runners.py $GIT_TOKEN -g $GROUP_ID -p $PROJECT_ID --tags $tag1,$tag2` will delete all runners with $tag1 AND $tag2 but not runners that have only one of these tags.
* `--stale_date`: Optional date in YYYY-MM-DD. If a runner has a `contacted_at` date before (older than) this date (or `None`), it will be deleted. Note that this will require one additional API call per runner in the group or project matching the `tags`, to get the runner's details (including their `contacted_at` date). This may cause a significant number of API calls and thus may be slow.
* `-n`, `--no-act`: Dryrun mode. Only list runners to be deleted, don't actually delete.
* `-l`, `--limit`: Optional maximum number of runners to delete
* `--gitlab`: URL of the GitLab instance. Defaults to `https://gitlab.com`
* `-s`, `--scope`: Only deletes runners of the specified scope. Valid values are "online", "offline", "active", and "paused"

## DISCLAIMER

This script is provided for educational purposes. It is **not supported by GitLab**. However, you can create an issue if you need help or propose a Merge Request. Be aware that once a runner is deleted, there is no way to undo that operation.
