#!/usr/bin/python3
import itertools
import argparse
import datetime
import gitlab
import pycurl
from io import BytesIO
from pprint import pprint

def disable_runners(gl, runners, where, stale_date, no_act, limit):
    all_runners = 0
    disabled_runners = 0
    for runner in runners:
        is_stale = False
        contact_date = None
        if limit is not None and disabled_runners >= limit:
            print("While disabling runners, deletion limit reached.")
            break

        if stale_date is not None:
            runner_object = gl.runners.get(runner_id)
            if runner_object.contacted_at is not None:
                contacted = datetime.datetime.strptime(runner_object.contacted_at, '%Y-%m-%dT%H:%M:%S.%fZ')
                contact_date = contacted.date()
                if contact_date < stale_date.date():
                    is_stale = True
            else:
                is_stale = True
            all_runners += 1
        try:
            if stale_date is None:
                if no_act:
                    print("Would disable runner %s:%s from %s" % (runner.id, runner.description, where))
                    disabled_runners += 1
                else:
                    headerdata = ["PRIVATE-TOKEN: {}".format(args.token)]
                    url = pycurl.Curl()
                    result = BytesIO()
                    url.setopt(url.URL, "https://gitlab.com/api/v4/projects/%s/runners/%s" % (where, runner.id))
                    url.setopt(pycurl.CUSTOMREQUEST, "DELETE")
                    url.setopt(pycurl.HTTPHEADER, headerdata)
                    url.setopt(url.WRITEDATA, result)
                    url.perform()
                    print("Disabled runner %s:%s from %s" % (runner.id, runner.description, where))
                    disabled_runners += 1
            else:
                if is_stale:
                    if no_act:
                        print("Would disable runner %s:%s, contacted at %s" % (runner.id, runner.description, contact_date))
                        disabled_runners += 1
                    else:
                        headerdata = ["PRIVATE-TOKEN: {}".format(args.token)]
                        url = pycurl.Curl()
                        result = BytesIO()
                        url.setopt(url.URL, "https://gitlab.com/api/v4/projects/%s/runners/%s" % (where, runner))
                        url.setopt(pycurl.CUSTOMREQUEST, "DELETE")
                        url.setopt(pycurl.HTTPHEADER, headerdata)
                        url.setopt(url.WRITEDATA, result)
                        url.perform()
                        print("Disabled runner %s:%s, contacted at %s" % (runner.id, runner.description, contact_date))
                        disabled_runners += 1
        except Exception as e:
            print("Unable to disable %s:%s in %s. Exception: %s" % (runner.id, runner.description, where, str(e)))

    if no_act:
        print("Would disable %s runners found in %s" % (disabled_runners, where))
    else:
        print("Disabled %s of %s runners found in %s" % (disabled_runners, all_runners, where))

def delete_runners(gl, runners, where, stale_date, no_act, limit):
    all_runners = 0
    deleted_runners = 0
    for runner in runners:
        is_stale = False
        contact_date = None

        if limit is not None and deleted_runners >= limit:
            print("Deletion limit reached.")
            break

        if stale_date is not None:
            runner_object = gl.runners.get(runner.id)
            if runner_object.contacted_at is not None:
                contacted = datetime.datetime.strptime(runner_object.contacted_at, '%Y-%m-%dT%H:%M:%S.%fZ')
                contact_date = contacted.date()
                if contact_date < stale_date.date():
                    is_stale = True
            else:
                is_stale = True
        all_runners += 1
        try:
            if stale_date is None:
                if no_act:
                    print("Would delete runner %s:%s" % (runner.id, runner.description))
                    deleted_runners += 1
                else:
                    gl.runners.delete(runner.id, retry_transient_errors=True)
                    print("Deleted runner %s:%s" % (runner.id, runner.description))
                    deleted_runners += 1
            else:
                if is_stale:
                    if no_act:
                        print("Would delete runner %s:%s, contacted at %s" % (runner.id, runner.description, contact_date))
                        deleted_runners += 1
                    else:
                        gl.runners.delete(runner.id , retry_transient_errors=True)
                        print("Deleted runner %s:%s, contacted at %s" % (runner.id, runner.description, contact_date))
                        deleted_runners += 1
        except Exception as e:
            print("Unable to delete %s:%s in %s. Exception: %s" % (runner.id, runner.description, where, str(e)))

    if no_act:
        print("Would delete %s runners found in %s" % (deleted_runners, where))
    else:
        print("Deleted %s of %s runners found in %s" % (deleted_runners, all_runners, where))

parser = argparse.ArgumentParser(description='Delete group runners')
parser.add_argument('token', help='API token able to read the requested group (Owner)')
parser.add_argument('-n', '--no-act', help='Print the runners that would be deleted, but take no action', action='store_true')
parser.add_argument('-g','--group', help='Group IDs to delete runners on', action='append')
parser.add_argument('-p','--project', help='Project IDs to delete runners on', action='append')
parser.add_argument('-l','--limit', help='Only delete [limit] number of runners', type=int, default=None)
parser.add_argument('--tags', help='Optional list of tags of runners to be deleted (comma separated)')
parser.add_argument('--stale_date', help='Oldest date of last contact. Runners with contacted_at before this date will be deleted. YYYY-MM-DD.')
parser.add_argument('--gitlab', help='The URL of the GitLab instance', default="https://gitlab.com")
parser.add_argument('-s', '--scope', help="Only delete runners of the specified scope", type=str, default=None)
args = parser.parse_args()

gl = gitlab.Gitlab(args.gitlab, private_token=args.token)

tags = args.tags if args.tags else None
if args.scope:
    kwargs = dict(scope = args.scope)
else:
    kwargs = dict(scope = None)
kwargs["tags"] = tags
kwargs["as_list"] = False
kwargs["retry_transient_errors"] = True


stale_date = None
if args.stale_date:
    print("WARN: Running this with stale_date will result in one additional API call per runner to get their contacted_at date. This may be slow.")
    try:
        stale_date = datetime.datetime.strptime(args.stale_date, '%Y-%m-%d')
        if stale_date > datetime.datetime.now():
            print("ERROR: Stale date may not be in the future")
            exit(1)
    except Exception as e:
        print("ERROR: Not a valid date: %s. Format must be YYYY-MM-DD" % args.stale_date)
        exit(0)

if args.group:
    kwargs["type"] = "group_type"
    for group in args.group:
        group = gl.groups.get(group, retry_transient_errors=True)
        runners = group.runners.list(**{k:v for k,v in kwargs.items() if v is not None})
        delete_runners(gl, runners, "group " + str(group.id), stale_date, args.no_act, args.limit)

if args.project:
    kwargs["type"] = "project_type"
    for project in args.project:
        project = gl.projects.get(project, retry_transient_errors=True)
        runners = project.runners.list(**{k:v for k,v in kwargs.items() if v is not None})
        runners_to_disable,runners_to_delete = itertools.tee(runners,2)
        disable_runners(gl, runners_to_disable, str(project.id), stale_date, args.no_act, args.limit)
        delete_runners(gl, runners_to_delete, str(project.id), stale_date, args.no_act, args.limit)

if not args.group and not args.project:
    kwargs["type"] = "instance_type"
    runners = gl.runners.all(**{k:v for k,v in kwargs.items() if v is not None})
    delete_runners(gl, runners, "instance", stale_date, args.no_act, args.limit)
